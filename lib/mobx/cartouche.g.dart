// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cartouche.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Cartouche on CartoucheBase, Store {
  final _$colorAtom = Atom(name: 'CartoucheBase.color');

  @override
  List get color {
    _$colorAtom.context.enforceReadPolicy(_$colorAtom);
    _$colorAtom.reportObserved();
    return super.color;
  }

  @override
  set color(List value) {
    _$colorAtom.context.conditionallyRunInAction(() {
      super.color = value;
      _$colorAtom.reportChanged();
    }, _$colorAtom, name: '${_$colorAtom.name}_set');
  }

  final _$backColorAtom = Atom(name: 'CartoucheBase.backColor');

  @override
  List get backColor {
    _$backColorAtom.context.enforceReadPolicy(_$backColorAtom);
    _$backColorAtom.reportObserved();
    return super.backColor;
  }

  @override
  set backColor(List value) {
    _$backColorAtom.context.conditionallyRunInAction(() {
      super.backColor = value;
      _$backColorAtom.reportChanged();
    }, _$backColorAtom, name: '${_$backColorAtom.name}_set');
  }

  final _$CartoucheBaseActionController =
      ActionController(name: 'CartoucheBase');

  @override
  void onClick(int number) {
    final _$actionInfo = _$CartoucheBaseActionController.startAction();
    try {
      return super.onClick(number);
    } finally {
      _$CartoucheBaseActionController.endAction(_$actionInfo);
    }
  }
}
