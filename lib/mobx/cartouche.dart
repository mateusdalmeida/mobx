import 'package:flutter/material.dart' as material;
import 'package:mobx/mobx.dart';

part 'cartouche.g.dart';

class Cartouche = CartoucheBase with _$Cartouche;

abstract class CartoucheBase with Store {
  @observable
  List color = List.generate(60, (i) => false);

  @observable
  List backColor = List.generate(60, (i) => material.Colors.transparent);

  @action
  void onClick(int number) {
    color[number] = !color[number];

    if (backColor[number] == material.Colors.transparent) {
      backColor[number] = material.Colors.red;
    } else {
      backColor[number] = material.Colors.transparent;
    }
  }
}
