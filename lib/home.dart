import 'package:flutter/material.dart';
import 'package:testes/mobx/cartouche.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _cartouche = Cartouche();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
          padding: EdgeInsets.fromLTRB(32, 0, 32, 32),
          child: Container(
            decoration: new BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8)),
            child: GridView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 60,
                padding: EdgeInsets.all(2),
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 6,
                    childAspectRatio: 1,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 2),
                itemBuilder: (BuildContext context, int index) {
                  return Observer(
                    builder: (_) => GestureDetector(
                      onTap: () {
                        _cartouche.onClick(index);
                        print(_cartouche.color[index]);
                        setState(() {});
                      },
                      child: Container(
                        decoration: new BoxDecoration(
                          color: _cartouche.backColor[index],
                          shape: BoxShape.circle,
                        ),
                        child: Center(
                          child: Text(
                            _cartouche.color[index].toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: _cartouche.color[index]
                                  ? Colors.white
                                  : Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          )),
    );
  }
}
